import 'package:flutter/material.dart';
import 'user.dart';
import 'push.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: LoginPage(),
  ));
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final User user = new User();

  @override
  Widget build(BuildContext context) {
    final TextField _txtUserName = new TextField(
      decoration: new InputDecoration(
        hintText: 'ชื่อ',
        contentPadding: new EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      keyboardType: TextInputType.text,
      autocorrect: false,
      onChanged: (text) {
        setState(() {
          this.user.userNameString = text;
        });
      },
    );

    final TextField _txtEmail = new TextField(
      decoration: new InputDecoration(
        hintText: 'สถานที่',
        contentPadding: new EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      keyboardType: TextInputType.text,
      autocorrect: false,
      onChanged: (text) {
        setState(() {
          this.user.emailString = text;
        });
      },
    );

    final TextField _txtPassword = new TextField(
      decoration: new InputDecoration(
        hintText: 'เบอร์โทร',
        contentPadding: new EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      keyboardType: TextInputType.text,
      autocorrect: false,
      obscureText: true,
      onChanged: (text) {
        setState(() {
          this.user.passwordString = text;
        });
      },
    );
    final TextField _txtPassword1 = new TextField(
      decoration: new InputDecoration(
        hintText: 'เวลา',
        contentPadding: new EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      keyboardType: TextInputType.text,
      autocorrect: false,
      onChanged: (text) {
        setState(() {
          this.user.emailString = text;
        });
      },
    );
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xFFFFC107),
        title: new Text("กรอกข้อมูล"),
      ),
      body: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            padding: EdgeInsets.fromLTRB(0, 0, 50, 20),
            width: 450.0,
            height: 300.0,
            child: new Image.asset(
                'images/120145073_696732517606689_2262095968981112114_n.png'),
          ),
          new Container(
            margin: new EdgeInsets.only(
                left: 20.0, right: 20.0, bottom: 0.0, top: 3.0),
            decoration: new BoxDecoration(
                color: Color.fromARGB(255, 240, 240, 240),
                border: new Border.all(width: 1.2, color: Colors.black26),
                borderRadius:
                    const BorderRadius.all(const Radius.circular(25.0))),
            child: _txtUserName,
          ),
          new Container(
            margin: new EdgeInsets.only(left: 20.0, right: 20.0, top: 3.0),
            decoration: new BoxDecoration(
                color: Color.fromARGB(255, 240, 240, 240),
                border: new Border.all(width: 1.2, color: Colors.black26),
                borderRadius:
                    const BorderRadius.all(const Radius.circular(25.0))),
            child: _txtEmail,
          ),
          new Container(
            margin: new EdgeInsets.only(
                left: 20.0, right: 20.0, bottom: 0.0, top: 3.0),
            decoration: new BoxDecoration(
                color: Color.fromARGB(255, 240, 240, 240),
                border: new Border.all(width: 1.2, color: Colors.black26),
                borderRadius:
                    const BorderRadius.all(const Radius.circular(25.0))),
            child: _txtPassword1,
          ),
          new Container(
            margin: new EdgeInsets.only(left: 20.0, right: 20.0, top: 3.0),
            decoration: new BoxDecoration(
                color: Color.fromARGB(255, 240, 240, 240),
                border: new Border.all(width: 1.2, color: Colors.black26),
                borderRadius:
                    new BorderRadius.all(const Radius.circular(25.0))),
            child: _txtPassword,
          ),
          new Container(
            margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 3.0),
            child: new Row(
              children: <Widget>[
                new Expanded(
                    child: new RaisedButton(
                  child: new Text("ยืนยัน"),
                  textColor: Colors.white,
                  color: Colors.red,
                  onPressed: () {
                    print('userNameString ==> ${user.userNameString}');
                    print('emailString ==> ${user.emailString}');
                    print('passwordString ==> ${user.passwordString}');
                  },
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
